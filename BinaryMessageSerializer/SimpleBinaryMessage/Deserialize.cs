﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

namespace BinaryMessageSerializer.Simple
{
    public partial class SimpleBinaryMessage : BinaryMessage
    {
        /// <summary>
        /// Десериализует граф объекта
        /// </summary>        
        /// <param name="stream">Поток, из которого происходит чтение байтов</param>
        public sealed override void Deserialize(Stream stream)
        {
            object item = this;

            PropertyInfo[] properties = item.GetType().GetProperties();

            for (int i = 0; i < properties.Count(); i++)
            {
                //Будет использовать для оперделения наличия метода Deserialize в классе
                MethodInfo method;

                #region Десериализация списков

                if (properties[i].PropertyType.IsGenericType && properties[i].PropertyType.isList())
                {
                    Int16 count = 0;
                    count = Convert.ToInt16(ReadTypeFromStream(typeof(Int16), stream));

                    if (count == 0)
                    {
                        item.GetType().GetProperty(properties[i].Name).SetValue(item, null, null);
                        continue;
                    }

                    item.GetType().GetProperty(properties[i].Name).SetValue(item, System.Activator.CreateInstance(properties[i].PropertyType), null);

                    object value = item.GetType().GetProperty(properties[i].Name).GetValue(item, null);

                    IList p = (IList)value;

                    method = properties[i].PropertyType.GetGenericArguments()[0].GetMethod("Deserialize");

                    for (int j = 0; j < count; j++)
                    {
                        if (method != null)
                        {
                            p.Add(System.Activator.CreateInstance(properties[i].PropertyType.GetGenericArguments()[0]));
                            method.Invoke(p[j], new object[] { stream });
                        }
                        else
                        {
                            p.Add(ReadTypeFromStream(properties[i].PropertyType.GetGenericArguments()[0], stream));
                        }
                    }

                    continue;
                } 
                #endregion

                #region Десериализация простых типов и классов

                method = properties[i].PropertyType.GetMethod("Deserialize");

                if (method == null)
                {
                    bool isHave = false;
                    isHave = (bool)ReadTypeFromStream(typeof(bool), stream);

                    if (isHave)
                        item.GetType().GetProperty(properties[i].Name).SetValue(item, ReadTypeFromStream(properties[i].PropertyType, stream), null);
                    else
                        item.GetType().GetProperty(properties[i].Name).SetValue(item, null, null);
                }
                else
                {
                    bool isHave = false;
                    isHave = (bool)ReadTypeFromStream(typeof(bool), stream);

                    if (isHave)
                    {
                        item.GetType().GetProperty(properties[i].Name).SetValue(item, System.Activator.CreateInstance(properties[i].PropertyType), null);
                        object value = item.GetType().GetProperty(properties[i].Name).GetValue(item, null);

                        method.Invoke(value, new object[] { stream });
                    }
                    else
                    {
                        item.GetType().GetProperty(properties[i].Name).SetValue(item, null, null);
                    }
                } 
                #endregion
            }
        }
    }
}