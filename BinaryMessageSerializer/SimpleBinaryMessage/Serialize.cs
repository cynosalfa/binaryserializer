﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

namespace BinaryMessageSerializer.Simple
{
    public partial class SimpleBinaryMessage : BinaryMessage
    {
        /// <summary>
        /// Сериализует в поток переданный граф объект
        /// </summary>       
        /// <param name="stream">Поток, в который происходит сериализация</param>
        public sealed override void Serialize(Stream stream)
        {
            object send = this;

            PropertyInfo[] properties = send.GetType().GetProperties();

            BinaryWriter bw = new BinaryWriter(stream);

            for (int i = 0; i < properties.Count(); i++)
            {
                //Будет использоваться для определения наличия метода Serialize в классе
                MethodInfo method;

                #region Сериализация списков

                if (properties[i].PropertyType.IsGenericType && properties[i].PropertyType.isList())
                {
                    object value = send.GetType().GetProperty(properties[i].Name).GetValue(send, null);

                    if (value != null)
                    {
                        IList p = (IList)value;
                        bw.Write(Convert.ToInt16(p.Count));

                        method = p[0].GetType().GetMethod("Serialize");

                        for (int j = 0; j < p.Count; j++)
                        {
                            if (method == null)
                            {
                                WriteTypeToStream(p[j], p[j].GetType(), stream);
                            }
                            else
                            {
                                method.Invoke(p[j], new object[] { stream });
                            }
                        }
                    }
                    else
                    {
                        Int16 len = 0;
                        bw.Write(len);
                    }

                    continue;
                } 
                #endregion

                #region Сериализация простых типов и классов

                method = properties[i].PropertyType.GetMethod("Serialize");

                if (method == null)
                {
                    object value = send.GetType().GetProperty(properties[i].Name).GetValue(send, null);

                    if (value != null)
                    {
                        bw.Write(true);

                        WriteTypeToStream(value, properties[i].PropertyType, stream);
                    }
                    else
                    {
                        bw.Write(false);
                    }
                }
                else
                {
                    object value = send.GetType().GetProperty(properties[i].Name).GetValue(send, null);

                    if (value != null)
                    {
                        bw.Write(true);

                        method.Invoke(value, new object[] { stream });
                    }
                    else
                    {
                        bw.Write(false);
                    }
                } 
                #endregion
            }
        }
    }
}