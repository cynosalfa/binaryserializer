﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BinaryMessageSerializer.Simple
{
    /// <summary>
    /// Класс, описывающий расширения типов
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Проверка на наследование интерфейса IList
        /// </summary>
        /// <param name="item">Экземпляр типа</param>
        /// <returns>Наследует или нет</returns>
        public static bool isList(this Type item)
        {
            return item.GetInterfaces().Any(iface => iface.GetGenericTypeDefinition() == typeof(IList<>));
        }
    }
}
