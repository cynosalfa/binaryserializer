﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace BinaryMessageSerializer
{
    public abstract class BinaryMessage
    {
        /// <summary>
        /// Записывает значение в поток
        /// </summary>
        /// <param name="value">Значние</param>
        /// <param name="t">Тип</param>
        /// <param name="str">Поток</param>
        protected void WriteTypeToStream(object value, Type t, Stream str)
        {
            byte[] bytes;

            BinaryWriter bw = new BinaryWriter(str);

            if (t == typeof(string))
            {
                bytes = Encoding.UTF8.GetBytes(value.ToString());
                bw.Write(Convert.ToInt16(bytes.Length));

            }
            else if (t == typeof(DateTime))
            {
                DateTime v = (DateTime)value;
                Int64 ticks = v.Ticks;
                bytes = BitConverter.GetBytes(ticks);
            }
            else if (t == typeof(Guid) || t == typeof(Guid?))
            {
                Guid g = (Guid)value;
                bytes = g.ToByteArray();
            }
            else
            {
                bytes = BitConverter.GetBytes((dynamic)value);
            }

            bw.Write(bytes);
        }
        /// <summary>
        /// Производит десериализацию из потока определенного типа данных
        /// </summary>
        /// <param name="t">Тип данных, которые нужно сдесериализовать</param>
        /// <param name="str">Поток, из которого осуществляется чтини байтов данных</param>
        /// <returns></returns>
        protected object ReadTypeFromStream(Type t, Stream str)
        {
            BinaryReader br = new BinaryReader(str, Encoding.UTF8);

            try
            {
                if (t == typeof(string))
                {
                    Int16 len = br.ReadInt16();

                    if (len == 0)
                        return null;
                    else
                        return Encoding.UTF8.GetString(br.ReadBytes(len));
                }
                else if (t == typeof(DateTime))
                {
                    long ticks = br.ReadInt64();

                    return new DateTime(ticks);
                }
                else if (t == typeof(decimal))
                {
                    return br.ReadDecimal();
                }
                else if (t == typeof(double))
                {
                    return br.ReadDouble();
                }
                else if (t == typeof(Int16))
                {
                    return br.ReadInt16();
                }
                else if (t == typeof(Int32))
                {
                    return br.ReadInt32();
                }
                else if (t == typeof(Int64))
                {
                    return br.ReadInt64();
                }
                else if (t == typeof(SByte))
                {
                    return br.ReadSByte();
                }
                else if (t == typeof(Single))
                {
                    return br.ReadSingle();
                }
                else if (t == typeof(UInt16))
                {
                    return br.ReadUInt16();
                }
                else if (t == typeof(UInt32))
                {
                    return br.ReadUInt32();
                }
                else if (t == typeof(UInt64))
                {
                    return br.ReadUInt64();
                }
                else if (t == typeof(UInt16))
                {
                    return br.ReadUInt16();
                }
                else if (t == typeof(Byte))
                {
                    return br.ReadByte();
                }
                else if (t == typeof(Char))
                {
                    return br.ReadChar();
                }
                else if (t == typeof(bool))
                {
                    return br.ReadBoolean();
                }
                else if (t == typeof(Guid))
                {
                    return new Guid(br.ReadBytes(16));
                }

                br.Close();
            }

            catch
            {
            }

            return "";
        }        
        /// <summary>
        /// Производит сериализацию графа объекта
        /// </summary>
        /// <param name="stream">Поток, в который необходимо сереализовать объект</param>
        public abstract void Serialize(Stream stream);
        /// <summary>
        /// Производит десериализацию графа объекта из потока
        /// </summary>
        /// <param name="stream">Поток, в котором находит сериализованный объект</param>
        public abstract void Deserialize(Stream stream);
    }
}
